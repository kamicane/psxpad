# PSXPad: PlayStation 1/2 gamepad library for Arduino

This library reads PlayStation 1/2 gamepads via SPI on Arduino.
Forked from [AZO's PSXPad](https://github.com/AZO234/Arduino_PSXPad).

## Features

- PS1 digital controllers support
- Dual Shock support in both digital and analog mode
- Dual Shock 2 support in both digital and analog mode
- Can read any button as an analog value with a DS2 controller
- Hot plug support, hot controller change support (swap a ds1 with a ds2)
- Dynamic mode change (analog button) support
- Vibration support

## Pins

PSX controller pins (looking at the gamepad connector)
```
 123 456 789
(...|...|...)
```

### for Arduino
    1 : DAT -> MISO (needs a 1kΩ pullup resistor to 5V)
    2 : CMD -> MOSI
    3 : VBR -> 7.6V (for vibration motors, works with 5V on some controllers, may be omitted)
    4 : GND -> GND
    5 : VCC -> 3.3V or 5V
    6 : ATT -> any digital pin
    7 : SCK -> SCLK
    8 : NC (not connected)
    9 : ACK (not in use)

### for ATmega88P/168P/328P 3.3V powered
    1 : DAT -> MISO (needs a 1kΩ pullup resistor to 3.3V)
    2 : CMD -> MOSI
    3 : VBR -> 7.6V (for vibration motors, may be omitted)
    4 : GND -> GND
    5 : VCC -> 3.3V
    6 : ATT -> any digital pin
    7 : SCK -> SCLK
    8 : NC (not connected)
    9 : ACK (not in use)

# References
 - [Interfacing a PS2 (PlayStation 2) Controller - CuriousInventor Tutorials](http://store.curiousinventor.com/guides/PS2/)
 - [Lynxmotion ps2 commands](http://www.lynxmotion.com/images/files/ps2cmd01.txt)

