#ifndef _PSXPAD_H_
#define _PSXPAD_H_

// PSXPad

#include <Arduino.h>

typedef enum {
  PSX_MODE_UNKNOWN = 0,
  PSX_MODE_DIGITAL,
  PSX_MODE_DS1,
  PSX_MODE_DS2
} PSXPad_Mode;

typedef enum {
  PSX_INPUT_CROSS = 0,
  PSX_INPUT_SQUARE,
  PSX_INPUT_CIRCLE,
  PSX_INPUT_TRIANGLE,

  PSX_INPUT_UP,
  PSX_INPUT_DOWN,
  PSX_INPUT_LEFT,
  PSX_INPUT_RIGHT,

  PSX_INPUT_START,
  PSX_INPUT_SELECT,

  PSX_INPUT_L1,
  PSX_INPUT_R1,
  PSX_INPUT_L2,
  PSX_INPUT_R2,
  PSX_INPUT_L3,
  PSX_INPUT_R3,

  PSX_INPUT_RX,
  PSX_INPUT_RY,
  PSX_INPUT_LX,
  PSX_INPUT_LY
} PSXPad_Inputs;

/* PSX pad class */
class PSXPad {
public:
  PSXPad(const unsigned int i_attPin);
  void query();

  void setMotorLeft(const byte i_motorLevel);
  void setMotorRight(const byte i_motorLevel);

  bool getButton(const PSXPad_Inputs i_input);
  byte getAnalog(const PSXPad_Inputs i_input);
  byte getAxis(const PSXPad_Inputs i_input);

  PSXPad_Mode getMode();

  static void setup();

  unsigned int cmdDelay;
  unsigned int spiDelay;
  byte mode;

private:
  void config(const bool i_setAnalog);

  unsigned int attPin;
  byte readCmd[21];
  byte inputData[21];
  byte responseData[21];

  byte motorLeft;
  byte motorRight;

  unsigned long lastCommand;

  void command(const byte i_cmd[], const unsigned int i_cmdLen);
};

#endif /* _PSXPAD_H_ */

