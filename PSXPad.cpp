#include "PSXPad.h"
#include <SPI.h>

const byte PSX_CMD_ENTER_CFG[]       = {0x01, 0x43, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00};
const byte PSX_CMD_EXIT_CFG[]        = {0x01, 0x43, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

const byte PSX_CMD_ENABLE_MOTOR[]    = {0x01, 0x4D, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF};

const byte PSX_CMD_SET_DS2[]         = {0x01, 0x4F, 0x00, 0xFF, 0xFF, 0x03, 0x00, 0x00, 0x00};

const byte PSX_CMD_SET_ANALOG_LOCK[] = {0x01, 0x44, 0x00, 0x01, 0x03, 0x00, 0x00, 0x00, 0x00};
const byte PSX_CMD_SET_ANALOG[]      = {0x01, 0x44, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00};

const byte PSX_CMD_GET_MODE_MODEL[]  = {0x01, 0x45, 0x00, 0x5A, 0x5A, 0x5A, 0x5A, 0x5A, 0x5A};
const byte PSX_CMD_GET_ANALOG_MODE[] = {0x01, 0x41, 0x00, 0x5A, 0x5A, 0x5A, 0x5A, 0x5A, 0x5A};

const byte PSX_CMD_READ[]            = {0x01, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

const byte PSX_BYTE_DIGITAL = 0x41;
const byte PSX_BYTE_DS1 = 0x73;
const byte PSX_BYTE_DS2 = 0x79;

PSXPad::PSXPad (const unsigned int i_attPin) {
  attPin = i_attPin;
  pinMode(attPin, OUTPUT);
  digitalWrite(attPin, HIGH);

  motorLeft = 0x00;
  motorRight = 0x00;
  mode = 0xFF;
  cmdDelay = 2000;
  spiDelay = 20;

  lastCommand = micros();
}

void PSXPad::command (const byte i_cmd[], const unsigned int i_cmdLen) {
  unsigned int i;
  byte cmd;

  unsigned long now = micros();
  unsigned long elapsed = now - lastCommand;

  if (elapsed < cmdDelay) delayMicroseconds(cmdDelay - elapsed);

  digitalWrite(attPin, LOW);
  delayMicroseconds(spiDelay);

  if (i_cmdLen == 0) {
    unsigned int len = 2;
    for(i = 0; i < len; i++) {
      cmd = i_cmd[i];
      responseData[i] = SPI.transfer(cmd);
      if (i == 1) {
        byte tmpMode = responseData[i];
        if (tmpMode == PSX_BYTE_DIGITAL) len = 5;
        else if (tmpMode == PSX_BYTE_DS1) len = 9;
        else if (tmpMode == PSX_BYTE_DS2) len = 21;
      }
      delayMicroseconds(spiDelay);
    }
  } else {
    for(i = 0; i < i_cmdLen; i++) {
      cmd = i_cmd[i];
      responseData[i] = SPI.transfer(cmd);
      delayMicroseconds(spiDelay);
    }
  }

  digitalWrite(attPin, HIGH);

  lastCommand = micros();
}

void PSXPad::query (void) {
  PSXPad_Mode prevMode = getMode();

  memcpy(readCmd, PSX_CMD_READ, sizeof(readCmd));

  readCmd[3] = motorRight;
  readCmd[4] = motorLeft;

  command(readCmd, 0);
  mode = responseData[1];
  PSXPad_Mode nextMode = getMode();

  if (nextMode != prevMode) {
    if ((nextMode == PSX_MODE_DS1 && prevMode == PSX_MODE_DIGITAL) || (nextMode == PSX_MODE_DIGITAL && prevMode == PSX_MODE_UNKNOWN)) {
      config(true);
      query();
      return;
    }
  }

  memcpy(inputData, responseData, sizeof(responseData));
}

void PSXPad::config (const bool i_setAnalog) {
  command(PSX_CMD_ENTER_CFG, sizeof(PSX_CMD_ENTER_CFG));
  if (i_setAnalog) {
    command(PSX_CMD_SET_ANALOG, sizeof(PSX_CMD_SET_ANALOG));
    command(PSX_CMD_SET_DS2, sizeof(PSX_CMD_SET_DS2));
    command(PSX_CMD_ENABLE_MOTOR, sizeof(PSX_CMD_ENABLE_MOTOR));
  }
  command(PSX_CMD_EXIT_CFG, sizeof(PSX_CMD_EXIT_CFG));
}

PSXPad_Mode PSXPad::getMode () {
  switch (mode) {
    case PSX_BYTE_DIGITAL:
      return PSX_MODE_DIGITAL;
    break;
    case PSX_BYTE_DS1:
      return PSX_MODE_DS1;
    break;
    case PSX_BYTE_DS2:
      return PSX_MODE_DS2;
    break;
  }

  return PSX_MODE_UNKNOWN;
}

void PSXPad::setMotorRight (const byte i_motorLevel) {
  motorRight = i_motorLevel > 0 ? 0xFF : 0x00;
}

void PSXPad::setMotorLeft (const byte i_motorLevel) {
  motorLeft = i_motorLevel;
}

byte PSXPad::getAxis (const PSXPad_Inputs i_input) {
  if (getMode() < PSX_MODE_DS1) return 0x7F;

  switch (i_input) {
    case PSX_INPUT_RX:
      return inputData[5];
    break;
    case PSX_INPUT_RY:
      return inputData[6];
    break;
    case PSX_INPUT_LX:
      return inputData[7];
    break;
    case PSX_INPUT_LY:
      return inputData[8];
    break;
  }

  return 0x7F;
}

byte PSXPad::getAnalog (const PSXPad_Inputs i_input) {
  if (getMode() != PSX_MODE_DS2) return getButton(i_input) ? 0xFF : 0x00;

  switch (i_input) {
    case PSX_INPUT_TRIANGLE:
      return inputData[13];
    break;
    case PSX_INPUT_CIRCLE:
      return inputData[14];
    break;
    case PSX_INPUT_CROSS:
      return inputData[15];
    break;
    case PSX_INPUT_SQUARE:
      return inputData[16];
    break;

    case PSX_INPUT_RIGHT:
      return inputData[9];
    break;
    case PSX_INPUT_LEFT:
      return inputData[10];
    break;
    case PSX_INPUT_UP:
      return inputData[11];
    break;
    case PSX_INPUT_DOWN:
      return inputData[12];
    break;

    case PSX_INPUT_L1:
      return inputData[17];
    break;
    case PSX_INPUT_R1:
      return inputData[18];
    break;
    case PSX_INPUT_L2:
      return inputData[19];
    break;
    case PSX_INPUT_R2:
      return inputData[20];
    break;
  }

  return 0x00;
}

bool PSXPad::getButton (PSXPad_Inputs i_input) {
  if (getMode() == PSX_MODE_UNKNOWN) return false;

  switch (i_input) {
    case PSX_INPUT_RIGHT:
      return (inputData[3] & 0x20U) ? false : true;
    break;
    case PSX_INPUT_LEFT:
      return (inputData[3] & 0x80U) ? false : true;
    break;
    case PSX_INPUT_UP:
      return (inputData[3] & 0x10U) ? false : true;
    break;
    case PSX_INPUT_DOWN:
      return (inputData[3] & 0x40U) ? false : true;
    break;

    case PSX_INPUT_START:
      return (inputData[3] & 0x08U) ? false : true;
    break;
    case PSX_INPUT_SELECT:
      return (inputData[3] & 0x01U) ? false : true;
    break;

    case PSX_INPUT_TRIANGLE:
      return (inputData[4] & 0x10U) ? false : true;
    break;
    case PSX_INPUT_CIRCLE:
      return (inputData[4] & 0x20U) ? false : true;
    break;
    case PSX_INPUT_CROSS:
      return (inputData[4] & 0x40U) ? false : true;
    break;
    case PSX_INPUT_SQUARE:
      return (inputData[4] & 0x80U) ? false : true;
    break;

    case PSX_INPUT_L1:
      return (inputData[4] & 0x04U) ? false : true;
    break;
    case PSX_INPUT_R1:
      return (inputData[4] & 0x08U) ? false : true;
    break;
    case PSX_INPUT_L2:
      return (inputData[4] & 0x01U) ? false : true;
    break;
    case PSX_INPUT_R2:
      return (inputData[4] & 0x02U) ? false : true;
    break;
    case PSX_INPUT_L3:
      return (inputData[3] & 0x02U) ? false : true;
    break;
    case PSX_INPUT_R3:
      return (inputData[3] & 0x04U) ? false : true;
    break;
  }

  return false;
}

void PSXPad::setup () {
  SPI.begin();
  // 250kHz, LSB first, CPOL=1/CPHA=1
  SPI.beginTransaction(SPISettings(250000, LSBFIRST, SPI_MODE3));
}
