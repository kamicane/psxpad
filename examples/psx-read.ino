#include <PSXPad.h>

const int ATT_PIN = 9;
char debug[255];

const PSXPad* pad;

void setup() {
  Serial.begin(115200);

  PSXPad::setup();
  pad = new PSXPad(ATT_PIN);
}

void loop() {

  pad->setMotorLeft(0); // byte
  pad->setMotorRight(255); // byte

  // query gamepad for button / axis data and send motor data
  pad->query();

  // # getAxis:
  // PSX_INPUT_LY
  // PSX_INPUT_RY
  // PSX_INPUT_LX
  // PSX_INPUT_RY

  byte analogLY = pad->getAxis(PSX_INPUT_LY);
  byte analogRY = pad->getAxis(PSX_INPUT_RY);

  // # getButton:
  // PSX_INPUT_CROSS
  // PSX_INPUT_SQUARE
  // PSX_INPUT_CIRCLE
  // PSX_INPUT_TRIANGLE

  // PSX_INPUT_UP
  // PSX_INPUT_DOWN
  // PSX_INPUT_LEFT
  // PSX_INPUT_RIGHT

  // PSX_INPUT_START
  // PSX_INPUT_SELECT

  // PSX_INPUT_L1
  // PSX_INPUT_R1
  // PSX_INPUT_L2
  // PSX_INPUT_R2
  // PSX_INPUT_L3
  // PSX_INPUT_R3

  bool buttonL2 = pad->getButton(PSX_INPUT_L2);

  bool buttonCr = pad->getButton(PSX_INPUT_CROSS);
  bool buttonCi = pad->getButton(PSX_INPUT_CIRCLE);
  bool buttonSq = pad->getButton(PSX_INPUT_SQUARE);
  bool buttonTr = pad->getButton(PSX_INPUT_TRIANGLE);

  bool buttonSt = pad->getButton(PSX_INPUT_START);
  bool buttonSl = pad->getButton(PSX_INPUT_SELECT);

  // # getAnalog:
  // all buttons minus PSX_INPUT_START and PSX_INPUT_SELECT.
  byte analogL2 = pad->getAnalog(PSX_INPUT_L2);

  PSXPad_Mode mode = pad->getMode();

  // possible values for mode
  // PSX_MODE_UNKNOWN = no controller connected. getButton always returns false, getAnalog always returns 0x00, getAxis always returns 0x7F (center)
  // PSX_MODE_DIGITAL = digital mode (analog button off) / digital only controller
  // PSX_MODE_DS1 = dual shock controller. no analog reads on buttons, but has axis and vibration
  // PSX_MODE_DS2 = dual shock 2 controller. has analog reads on buttons, axis and vibration

  // in PSX_MODE_DIGITAL mode, getAnalog returns either 0x00 (off) or 0xFF (on).
  // in PSX_MODE_DIGITAL mode, getAxis returns 0x7F (center).

  sprintf(debug, "MODE: %02X ST: %02X SL: %02X CR: %02X TR: %02X CI: %02X SQ: %02X L2: %02X L2A: %02X", mode, buttonSt, buttonSl, buttonCr, buttonTr, buttonCi, buttonSq, buttonL2, analogL2);
  Serial.println(str);
}
